/**
 * Created by dc on 3/27/17.
 */

import com.google.inject.servlet.ServletModule;

public class MyServletModule extends ServletModule{
    protected void configureServlets(){
        serve("/").with(MyServlet.class);
    }

}
